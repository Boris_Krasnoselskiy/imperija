$(document).ready(function() {
	$(".js_open_this").on("click", function() {
		$(this).siblings("i").toggleClass("rotated");

		$(this).closest(".info").find(".text").toggleClass("opened");

      	$(this).text(function(i, text){
          return text === "Подробнее" ? "Скрыть" : "Подробнее";
      	})
	});
});